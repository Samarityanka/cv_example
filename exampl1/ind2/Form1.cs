﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ind2
{
    public partial class Form1 : Form
    {
        //объявление переменных для подсчета меток
        int mal = 0;
        int bol = 0;


        public Form1()
        {
            InitializeComponent();

        }
               
           private void button1_Click(object sender, EventArgs e)
        {
            //присвоение переменным значений введенным пользователем
            //переменные для задания положения метки
            int x = int.Parse(textBox3.Text);
            int y = int.Parse(textBox4.Text);
            //переменные для задания размеров метки
            int size_x = int.Parse(textBox1.Text);
            int size_y = int.Parse(textBox2.Text);

          //создание метки и ее свойств
            Label l = new Label();
            l.Text = "Variant 15 Kim";
            l.Parent = this;
            l.BorderStyle = BorderStyle.FixedSingle;
            l.Location = new Point(x, y);
            l.Size = new Size(size_x, size_y);
            //подсчет больших и маленьких меток
            if (size_x <= 50 && size_y <= 50)
                mal++;            
            else
                bol++;  
            
            //выведение количества меток в заголовок окна           
            string mal_st = "Количество маленьких меток: " + Convert.ToString(mal)+" Количество больших меток: "+Convert.ToString(bol);
            Form1.ActiveForm.Text = mal_st;
          
        }    

    }
}
