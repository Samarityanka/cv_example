import sys
import matplotlib.pyplot as plt
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import (QWidget, QPushButton, QLineEdit,
    QInputDialog, QApplication,QScrollArea, QMainWindow,QFileDialog,  QWidget,QLabel)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QImage,QPixmap

def fhist(img):        
        lred=[]
        lgreen=[]
        lblue=[]
        cred=[]
        cgreen=[]
        cblue=[]
        for i in range(img.width()):
            for j in range(img.height()):
                lred.append(img.pixelColor(i,j).red())
                lgreen.append(img.pixelColor(i,j).green())
                lblue.append(img.pixelColor(i,j).blue())
        for i in range(256):
            cred.append(lred.count(i))
            cgreen.append(lgreen.count(i))
            cblue.append(lblue.count(i))
        pmap=[]
        pmap.append(cred)
        pmap.append(cgreen)
        pmap.append(cblue)        
        return pmap

def fop(fileName):        
    img=QImage()        
    img.load(fileName)
    return img
    
def fcopy(img):
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    for i in range(w):
        for j in range(h):
            img2.setPixelColor(i,j,img.pixelColor(i,j))
    _im=[img2,2]
    return _im

def fdvi(offx,offy,img):
    cl=QColor()
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    for i in range(w):
        for j in range(h):
            cl.setRgb(127,127,127,255)
            img2.setPixelColor(i,j,cl)
    for i in range(w):
        for j in range(h):
            img2.setPixelColor(i+offx,j+offy,img.pixelColor(i,j))
    _im=[img2,2]
    return _im

def fadj(k,img,tp):
    if tp==1:
        cl=QColor()                
        w=img.width()
        h=img.height()
        img2=QImage(w,h,QImage.Format_RGB32)
        for i in range(w):
            for j in range(h):
                _r=img.pixelColor(i,j).red()*k
                _g=img.pixelColor(i,j).green()*k
                _b=img.pixelColor(i,j).blue()*k
                r=_r
                g=_g
                b=_b
                if r>255:
                    r=255
                if g>255:
                    g=255
                if b>255:
                    b=255
                if r<0:
                    r=0
                if g<0:
                    g=0
                if b<0:
                    b=0
                cl.setRgb(r,g,b,255)
                img2.setPixelColor(i,j,cl)
        _im=[img2,1]
    if tp==2:
        cl=QColor()                
        w=img.width()
        h=img.height()
        img2=QImage(w,h,QImage.Format_RGB32)
        for i in range(w):
            for j in range(h):
                #print('1')
                f= int(img.pixelColor(i,j).red()+img.pixelColor(i,j).green()+img.pixelColor(i,j).blue())/3
                _r=(img.pixelColor(i,j).red()-127)*k+127
                _g=(img.pixelColor(i,j).green()-127)*k+127
                _b=(img.pixelColor(i,j).blue()-127)*k+127

                #print('2')
                r=int(_r)
                g=int(_g)
                b=int(_b)
                if r>255:
                    r=255
                if g>255:
                    g=255
                if b>255:
                    b=255
                if r<0:
                    r=0
                if g<0:
                    g=0
                if b<0:
                    b=0
                #print('3')
                cl.setRgb(r,g,b,255)
                img2.setPixelColor(i,j,cl)
        _im=[img2,2]
    return _im

def fchb(img):
    cl=QColor()                
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    
    for i in range(w):
        for j in range(h):
            f=int((img.pixelColor(i,j).red()+img.pixelColor(i,j).green()+img.pixelColor(i,j).blue())/3)
            if f>255:
                f=255
            if f<0:
                f=0
            cl.setRgb(f,f,f,255)
            img2.setPixelColor(i,j,cl)
    _im=[img2,1]
    return _im

def fbin(k,img):          
    cl=QColor()        
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    for i in range(w):
        for j in range(h):
            _r=img.pixelColor(i,j).red()
            _g=img.pixelColor(i,j).green()
            _b=img.pixelColor(i,j).blue()
            r=_r
            g=_g
            b=_b
            f=int((r+g+b)/3)
            if f>k:
                f=255
            if f<k:
                f=0
            cl.setRgb(f,f,f,255)
            img2.setPixelColor(i,j,cl)
    _im=[img2,1]
    return _im

def frazn(img,img1):
    f_min=255
    f_min_r=255
    f_min_g=255
    f_min_b=255
    f_max_r=-255
    f_max_g=-255
    f_max_b=-255
    
    f_max=-255
    cl=QColor()                        
    if img.width()>=img1.width():
        w=img.width()
    else:
        w=img1.width()
    if img.height()>=img1.height():
        h=img.height()
    else:
        h=img1.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    
    
    for i in range(w):
        for j in range(h):
            _r=img.pixelColor(i,j).red()-img1.pixelColor(i,j).red()
            _g=img.pixelColor(i,j).green()-img1.pixelColor(i,j).green()
            _b=img.pixelColor(i,j).blue()-img1.pixelColor(i,j).blue()
            r=_r+127
            g=_g+127
            b=_b+127
            if r>255:
                r=255
            if g>255:
                g=255
            if b>255:
                b=255
            if r<0:
                r=0
            if g<0:
                g=0
            if b<0:
                b=0
            cl.setRgb(r,g,b,255)
            img2.setPixelColor(i,j,cl)
    _im=[img2,2]
    return _im

def fsum(img,img1):
    cl=QColor()       
    if img.width()>=img1.width():
        w=img.width()
    else:
        w=img1.width()
    if img.height()>=img1.height():
        h=img.height()
    else:
        h=img1.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    for i in range(w):
        for j in range(h):
            r=img.pixelColor(i,j).red()+img1.pixelColor(i,j).red()
            g=img.pixelColor(i,j).green()+img1.pixelColor(i,j).green()
            b=img.pixelColor(i,j).blue()+img1.pixelColor(i,j).blue()
            if r>255:
                r=255
            if g>255:
                g=255
            if b>255:
                b=255
            if r<0:
                r=0
            if g<0:
                g=0
            if b<0:
                b=0
            cl.setRgb(r,g,b,255)
            img2.setPixelColor(i,j,cl)
            
    _im=[img2,1]
    return _im

def flapl(img):       
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    cl=QColor()
    pix1=[[]]
    for i in range(img.width()+2):
        pix1.append([])
        for j in range(img.height()+2):
            if i==0 or i==img.width()+1 or j==0 or j==img.height()+1:
                pix1[i].append(127)
            else:                    
                pix1[i].append(int((img.pixelColor(i-1,j-1).red()+img.pixelColor(i-1,j-1).green()+img.pixelColor(i-1,j-1).blue())/3))       
    for i in range(img.width()):
        for j in range(img.height()):
            z1 = pix1[i][j]
            z2 = pix1[i+1][j]
            z3 = pix1[i+2][j]
            z4 = pix1[i][j+1]
            z5 = pix1[i+1][j+1]
            z6 = pix1[i+2][j+1]
            z7 = pix1[i][j+2]   
            z8 = pix1[i+1][j+2]
            z9 = pix1[i+2][j+2]
            z5=-z5*8
            f=z1+z2+z3+z4+z5+z6+z7+z8+z9+127
            if f<0:
                f=0
            if f>255:
                f=255
            cl.setRgb(f,f,f,255)
            img2.setPixelColor(i,j,cl)
            
    _im=[img2,2]
    return _im

def fprt(img):       
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    cl=QColor()
    pix1=[[]]
    for i in range(img.width()+2):
        pix1.append([])
        for j in range(img.height()+2):
            if i==0 or i==img.width()+1 or j==0 or j==img.height()+1:
                pix1[i].append(127)
            else:                    
                pix1[i].append(int((img.pixelColor(i-1,j-1).red()+img.pixelColor(i-1,j-1).green()+img.pixelColor(i-1,j-1).blue())/3))         
    for i in range(img.width()):
        for j in range(img.height()):
            z1 = pix1[i][j]
            z2 = pix1[i+1][j]
            z3 = pix1[i+2][j]
            z4 = pix1[i][j+1]
            z5 = pix1[i+1][j+1]
            z6 = pix1[i+2][j+1]
            z7 = pix1[i][j+2]   
            z8 = pix1[i+1][j+2]
            z9 = pix1[i+2][j+2]
            z5=-z5*4
            gx=(z7+z8+z9)-(z1+z2+z3)
            gy=(z3+z6+z9)-(z1+z4+z7)                
            f=int((gx*gx+gy*gy)**0.5)
            if f<0:
                f=0
            if f>255:
                f=255
            cl.setRgb(f,f,f,255)
            img2.setPixelColor(i,j,cl)
    _im=[img2,1]
    return _im

def fsob(img):       
    w=img.width()
    h=img.height()
    img2=QImage(w,h,QImage.Format_RGB32)
    cl=QColor()
    pix1=[[]]
    for i in range(img.width()+2):
        pix1.append([])
        for j in range(img.height()+2):
            if i==0 or i==img.width()+1 or j==0 or j==img.height()+1:
                pix1[i].append(127)
            else:                    
                pix1[i].append(int((img.pixelColor(i-1,j-1).red()+img.pixelColor(i-1,j-1).green()+img.pixelColor(i-1,j-1).blue())/3))        
    for i in range(img.width()):
        for j in range(img.height()):
            z1 = pix1[i][j]
            z2 = pix1[i+1][j]
            z3 = pix1[i+2][j]
            z4 = pix1[i][j+1]
            z5 = pix1[i+1][j+1]
            z6 = pix1[i+2][j+1]
            z7 = pix1[i][j+2]   
            z8 = pix1[i+1][j+2]
            z9 = pix1[i+2][j+2]
            z5=-z5*4
            gx=(z7+2*z8+z9)-(z1+2*z2+z3)
            gy=(z3+2*z6+z9)-(z1+2*z4+z7)                
            f=int((gx*gx+gy*gy)**0.5)
            if f<0:
                f=0
            if f>255:
                f=255               
            cl.setRgb(f,f,f,255)
            img2.setPixelColor(i,j,cl)
    _im=[img2,1]
    return _im

class PassWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle("Skullteam&kosbka_prod")
        self.resize(1240, 690)
        self.build()
    def build(self):
        self.but=[]
        self.butc=[]
        self.lab=[]
        self.scA=[]
        self.Ram=[]
        self.Ram1=[]
        
        self.image=[0,0,0]
        self.tp1=[1,1,1]
        for i in range (3):
            self._ram=QLabel(self)
            self._ram.setGeometry(5+i*400,5,390,390)
            self._ram.setStyleSheet('border:4px solid #0000ff')
            self._ram.setVisible(False)
            self._ram1=QLabel(self)
            self._ram1.setGeometry(5+i*400,5,390,390)
            self._ram1.setStyleSheet('border:2px solid #ff0000')
            self._ram1.setVisible(False)
            self.Ram.append(self._ram)
            self.Ram1.append(self._ram1)
            self._lab=QLabel(self)
            self._lab.setAlignment(Qt.AlignCenter)
            self.lab.append(self._lab)
            self._scA=QScrollArea(self)
            self._scA.setGeometry(10+i*400,10,380,380)
            self._scA.setWidget(self._lab)
            self.scA.append(self._scA)
                   
        self._but = QtWidgets.QPushButton('Загрузить изображение', self)
        self._but.setGeometry(QtCore.QRect(10, 410, 150, 40))
        self._but.clicked.connect(self.bfop)
        
        self.but1 = QtWidgets.QPushButton('Скопировать', self)
        self.but1.setGeometry(QtCore.QRect(10, 450, 120, 40))
        self.but1.clicked.connect(self.bfcopy)

        self.but2 = QtWidgets.QPushButton('Гистограмма', self)
        self.but2.setGeometry(QtCore.QRect(10, 490, 120, 40))
        self.but2.clicked.connect(self.bfhist)

        self.but8 = QtWidgets.QPushButton('Бинаризация', self)
        self.but8.setGeometry(QtCore.QRect(10, 530, 120, 40))
        self.but8.clicked.connect(self.bfbin)

        self.but9 = QtWidgets.QPushButton('Оператор Прюитта', self)
        self.but9.setGeometry(QtCore.QRect(10, 570, 120, 40))
        self.but9.clicked.connect(self.bfprt)

        self.but3 = QtWidgets.QPushButton('Оператор Собеля', self)
        self.but3.setGeometry(QtCore.QRect(10, 610, 120, 40))
        self.but3.clicked.connect(self.bfsob)

        self.but4 = QtWidgets.QPushButton('Оператор Лапласа', self)
        self.but4.setGeometry(QtCore.QRect(10, 650, 120, 40))
        self.but4.clicked.connect(self.bflapl)

        self.but5 = QtWidgets.QPushButton('Сумма', self)
        self.but5.setGeometry(QtCore.QRect(270, 450, 120, 40))
        self.but5.clicked.connect(self.bfsum)
        
        self.but6 = QtWidgets.QPushButton('Умножение', self)
        self.but6.setGeometry(QtCore.QRect(270, 490, 120, 40))
        self.but6.clicked.connect(self.bfadj)

        self.but7 = QtWidgets.QPushButton('Разность', self)
        self.but7.setGeometry(QtCore.QRect(270, 530, 120, 40))
        self.but7.clicked.connect(self.bfrazn)

        self.but8 = QtWidgets.QPushButton('Полутоновое', self)
        self.but8.setGeometry(QtCore.QRect(270, 570, 120, 40))
        self.but8.clicked.connect(self.bfchb)

        self.buts=[[0,0,0],[0,0,0],[0,0,0]]
        for i in range(3):
            for j in range(3):
                self.buts[i][j] = QtWidgets.QPushButton('', self)
                self.buts[i][j].setGeometry(QtCore.QRect(540+40*i, 450+40*j, 40, 40))
                self.buts[i][j].clicked.connect(self.bfdvi)
                self.buts[i][j].setAccessibleName(str(i)+str(j))                
                self.buts[i][j].setIcon(QtGui.QIcon(str(j)+str(i)+'_rotated.png'))
        self.buts[1][1].setVisible(False)
            
        self.lin1 = QtWidgets.QLineEdit(self)
        self.lin1.setGeometry(QtCore.QRect(130, 531, 120, 38))
        self.lin1.setText('')

        self.lin2 = QtWidgets.QLineEdit(self)
        self.lin2.setGeometry(QtCore.QRect(390, 491, 120, 38))
        self.lin2.setText('')

        self.lin4 = QtWidgets.QLineEdit(self)
        self.lin4.setGeometry(QtCore.QRect(580, 490, 40, 40))
        self.lin4.setText('')

        self.selec1=''
        self.selec2=''
    def bfchb(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0:
            if self.image[tx]!=0:
                f=fchb(self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))
    def bfhist(self):
        tx=self.selec1        
        ct=[]
        for i in range(256):
            ct.append(i)
        if tx!='':
            p=fhist(self.image[tx])
            plt.plot(ct,p[0],ct,p[1],ct,p[2])
            plt.show()
    
    def bfop(self):
        tx=self.selec1
        if tx!='':
            fileName =QFileDialog.getOpenFileName(self,'Открыть изображение','','Images (*.png *.jpg *.jpeg *.bmp)')[0]
            self.image[tx]=fop(fileName)
            self.tp1[tx]=1
            self.lab[tx].setGeometry(10,10,self.image[tx].width(),self.image[tx].height())
            self.lab[tx].setPixmap(QPixmap().fromImage(self.image[tx]))
    
    def bfcopy(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0:
            if self.image[tx]!=0:
                f=fcopy(self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))

    def bfdvi(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0 and self.lin4.text()!='':
            if self.image[tx]!=0:
            #if tx1!=tx:
                but=self.sender()        
                offx=int(self.lin4.text())*(int(but.accessibleName()[0])-1)
                offy=int(self.lin4.text())*(int(but.accessibleName()[1])-1)
                f=fdvi(offx,
                       offy,
                       self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))

    def bfadj(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0 and self.lin2.text()!='':
            if self.image[tx]!=0:
                print('5')
                print(self.tp1[tx])
                f=fadj(float(self.lin2.text()),
                       self.image[tx],
                       self.tp1[tx])
                print('6')
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))

    def bfbin(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0 and self.lin1.text()!='':
            if self.image[tx]!=0:
                f=fbin(float(self.lin1.text()),
                       self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))

    def bfrazn(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='':
            if self.image[tx]!=0 and self.image[tx1]!=0:
                tx2=2                          
                f=frazn(self.image[tx],
                        self.image[tx1])
                self.image[tx2]=f[0]
                self.tp1[tx2]=f[1]
                self.lab[tx2].setGeometry(10,10,self.image[tx2].width(),self.image[tx2].height())
                self.lab[tx2].setPixmap(QPixmap().fromImage(self.image[tx2]))        

    def bfsum(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='':
            if self.image[tx]!=0 and self.image[tx1]!=0:
                tx2=2 
                f=fsum(self.image[tx],
                       self.image[tx1])
                self.image[tx2]=f[0]
                self.tp1[tx2]=f[1]
                self.lab[tx2].setGeometry(10,10,self.image[tx2].width(),self.image[tx2].height())
                self.lab[tx2].setPixmap(QPixmap().fromImage(self.image[tx2]))

    def bflapl(self):
        tx=self.selec1
        tx1=self.selec2        
        if tx!='' and tx1!='' and tx1!=0:
            if self.image[tx]!=0:
                f=flapl(self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))
        
    def bfprt(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0:
            if self.image[tx]!=0:
                f=fprt(self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))
        
    def bfsob(self):
        tx=self.selec1
        tx1=self.selec2
        if tx!='' and tx1!='' and tx1!=0:
            if self.image[tx]!=0:
                f=fsob(self.image[tx])
                self.image[tx1]=f[0]
                self.tp1[tx1]=f[1]
                self.lab[tx1].setGeometry(10,10,self.image[tx1].width(),self.image[tx1].height())
                self.lab[tx1].setPixmap(QPixmap().fromImage(self.image[tx1]))        


#----------------------------------------------------------------------------------------------------

        
    def mousePressEvent(self, e):
        offx=20
        offy=20
        if e.y()>10+offy and e.y()<390-offy:
            if e.button() == Qt.LeftButton:
                if e.x()>10+offx and e.x()<390-offx:
                    if self.selec1!=0:
                        self.selec1=0
                        self.Ram[0].setVisible(True)
                        self.Ram[1].setVisible(False)
                        self.Ram[2].setVisible(False)
                    else:
                        self.selec1=''
                        self.Ram[0].setVisible(False)
                        self.Ram[1].setVisible(False)
                        self.Ram[2].setVisible(False)

                elif e.x()>410+offx and e.x()<790-offx:
                    if self.selec1!=1:
                        self.selec1=1
                        self.Ram[1].setVisible(True)
                        self.Ram[0].setVisible(False)
                        self.Ram[2].setVisible(False)
                    else:
                        self.selec1=''
                        self.Ram[1].setVisible(False)
                        self.Ram[0].setVisible(False)
                        self.Ram[2].setVisible(False)

                elif e.x()>810+offx and e.x()<1190-offx:
                    if self.selec1!=2:
                        self.selec1=2
                        self.Ram[2].setVisible(True)
                        self.Ram[1].setVisible(False)
                        self.Ram[0].setVisible(False)
                    else:
                        self.selec1=''
                        self.Ram[2].setVisible(False)
                        self.Ram[1].setVisible(False)
                        self.Ram[0].setVisible(False)
            if e.button() == Qt.RightButton:
                if e.x()>10+offx and e.x()<390-offx:
                    if self.selec2!=0:
                        self.selec2=0
                        self.Ram1[0].setVisible(True)
                        self.Ram1[1].setVisible(False)
                        self.Ram1[2].setVisible(False)
                    else:
                        self.selec2=''
                        self.Ram1[0].setVisible(False)
                        self.Ram1[1].setVisible(False)
                        self.Ram1[2].setVisible(False)

                elif e.x()>410+offx and e.x()<790-offx:
                    if self.selec2!=1:
                        self.selec2=1
                        self.Ram1[1].setVisible(True)
                        self.Ram1[0].setVisible(False)
                        self.Ram1[2].setVisible(False)
                    else:
                        self.selec2=''
                        self.Ram1[1].setVisible(False)
                        self.Ram1[0].setVisible(False)
                        self.Ram1[2].setVisible(False)

                elif e.x()>810+offx and e.x()<1190-offx:
                    if self.selec2!=2:
                        self.selec2=2
                        self.Ram1[2].setVisible(True)
                        self.Ram1[1].setVisible(False)
                        self.Ram1[0].setVisible(False)
                    else:
                        self.selec2=''
                        self.Ram1[2].setVisible(False)
                        self.Ram1[1].setVisible(False)
                        self.Ram1[0].setVisible(False)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = PassWindow()
    window.show()
    sys.exit(app.exec_())
